package com.app.freshcleannow.util

import android.content.Context
import android.content.SharedPreferences

object SharedPrefUtil {

    val DEFAULT_INT = 0
    val DEFAULT_LONG: Long = -1001
    val DEFAULT_FLOAT = -1.0f
    private var sharedPreferences: SharedPreferences? = null
    private val SHARED_PREF_NAME = "SharedPreference"

    private fun initSharedPreferences(context: Context) {
        sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
    }

    /**
     *
     *  To ensure that sharedPreferences has been initialized & it's not null.
     *
     * @param context [NonNull] context of App or any component of App.
     *
     */
    private fun ensureSharedPreferences(context: Context) {
        if (sharedPreferences == null) {
            initSharedPreferences(context)
            if (sharedPreferences == null) {
                throw RuntimeException("sharedPreferences couldn't be initialized from [$context]")
            }
        }
    }

    /**
     * helper methods to write data into common shared preferences file
     *
     *  *  [writeStringToSharedPreference]
     *  *  [writeBooleanToSharedPreference]
     *  *  [writeIntToSharedPreference]
     *  *  [writeLongToSharedPreference]
     *  *  [writeFloatToSharedPreference]
     *
     */
    fun writeStringToSharedPreference(context: Context, key: String, value: String?) {
        ensureSharedPreferences(context)
        sharedPreferences!!.edit().putString(key, value).apply()
    }

    fun writeBooleanToSharedPreference(context: Context, key: String, value: Boolean) {
        ensureSharedPreferences(context)
        sharedPreferences!!.edit().putBoolean(key, value).apply()
    }

    fun writeIntToSharedPreference(context: Context, key: String, value: Int) {
        ensureSharedPreferences(context)
        sharedPreferences!!.edit().putInt(key, value).apply()
    }

    fun writeLongToSharedPreference(context: Context, key: String, value: Long) {
        ensureSharedPreferences(context)
        sharedPreferences!!.edit().putLong(key, value).apply()
    }

    fun writeFloatToSharedPreference(context: Context, key: String, value: Float) {
        ensureSharedPreferences(context)
        sharedPreferences!!.edit().putFloat(key, value).apply()
    }

    /**
     * helper methods to read data from a common shared preferences file
     *
     *  *  [readStringFromSharedPreference]
     *  *  [readStringFromSharedPreference]
     *  *  [readBooleanFromSharedPreference]
     *  *  [readBooleanFromSharedPreference]
     *  *  [readIntFromSharedPreference]
     *  *  [readIntFromSharedPreference]
     *  *  [readLongFromSharedPreference]
     *  *  [readLongFromSharedPreference]
     *  *  [readFloatFromSharedPreference]
     *  *  [readFloatFromSharedPreference]
     *
     */
    fun readStringFromSharedPreference(context: Context, key: String): String? {
        return readStringFromSharedPreference(context, key, "")
    }

    fun readStringFromSharedPreference(
        context: Context,
        key: String,
        defaultValue: String
    ): String? {
        ensureSharedPreferences(context)
        return sharedPreferences!!.getString(key, defaultValue)
    }

    fun readBooleanFromSharedPreference(context: Context, key: String): Boolean {
        return readBooleanFromSharedPreference(context, key, false)
    }

    fun readBooleanFromSharedPreference(
        context: Context,
        key: String,
        defaultValue: Boolean
    ): Boolean {
        ensureSharedPreferences(context)
        return sharedPreferences!!.getBoolean(key, defaultValue)
    }

    fun readIntFromSharedPreference(context: Context, key: String): Int {
        return readIntFromSharedPreference(context, key, DEFAULT_INT)
    }

    fun readIntFromSharedPreference(context: Context, key: String, defaultValue: Int): Int {
        ensureSharedPreferences(context)
        return sharedPreferences!!.getInt(key, defaultValue)
    }

    fun readLongFromSharedPreference(context: Context, key: String): Long {
        return readLongFromSharedPreference(context, key, DEFAULT_LONG)
    }

    fun readLongFromSharedPreference(context: Context, key: String, defaultValue: Long): Long {
        ensureSharedPreferences(context)
        return sharedPreferences!!.getLong(key, defaultValue)
    }

    fun readFloatFromSharedPreference(context: Context, key: String): Float {
        return readFloatFromSharedPreference(context, key, DEFAULT_FLOAT)
    }

    fun readFloatFromSharedPreference(context: Context, key: String, defaultValue: Float): Float {
        ensureSharedPreferences(context)
        return sharedPreferences!!.getFloat(key, defaultValue)
    }

    /**
     * To remove a key from SharedPreference
     */
    fun removeKeyItem(context: Context, key: String): Boolean {
        ensureSharedPreferences(context)
        return sharedPreferences!!.edit().remove(key).commit()
    }

    /**
     * To clear all the SharedPreference data
     */
    fun clear(context: Context): Boolean {
        ensureSharedPreferences(context)
        return sharedPreferences!!.edit().clear().commit()
    }
}