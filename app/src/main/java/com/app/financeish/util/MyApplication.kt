package com.app.financeish.util

import android.app.Application

class MyApplication : Application(){

    companion object {
        @JvmField
        var appInstance: MyApplication? = null

        @JvmStatic
        fun getAppInstance(): MyApplication {
            return appInstance as MyApplication
        }
    }

    override fun onCreate() {
        super.onCreate()
        appInstance = this

    }
}