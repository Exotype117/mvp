package com.app.financeish.util

import android.widget.Toast

object GlobalFunction {

    fun showToast(message: String) {
        Toast.makeText(MyApplication.getAppInstance(), message, Toast.LENGTH_SHORT).show()
    }
}