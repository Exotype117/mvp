package com.app.financeish.util

object Constants {

    val EXPENSES = "expenses"
    val DEBIT = "debit"
    val INCOME ="income"
    val TYPE ="type"
}