package com.app.freshcleannow.util

import android.content.Context

object SharedPrefHelper {

    private val IS_LOGGED_IN = "is_logged_in"

    fun setIsLoggedIn(context: Context, isLoggedIn: Boolean) {
        SharedPrefUtil.writeBooleanToSharedPreference(context, IS_LOGGED_IN, isLoggedIn)
    }

    fun getIsLoggedIn(context: Context): Boolean {
        return SharedPrefUtil.readBooleanFromSharedPreference(context, IS_LOGGED_IN)
    }

}