package com.app.financeish.view.activities

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowManager
import com.app.financeish.R
import com.app.freshcleannow.util.SharedPrefHelper

class SplashActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            if (SharedPrefHelper.getIsLoggedIn(this)) {
                gotoMainScreen()
            } else {
                goToLoginScreen()
            }
        }, 2000)
    }



}