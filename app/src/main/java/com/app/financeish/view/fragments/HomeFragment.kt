package com.app.financeish.view.fragments

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.financeish.R
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.github.mikephil.charting.utils.ColorTemplate
import java.util.ArrayList


class HomeFragment : BaseFragment() {

    private var chart: LineChart? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.layout_home_fragment, container, false)

        chart = view.findViewById(R.id.chart) as LineChart
        chart!!.description.isEnabled = false
        chart!!.setDrawGridBackground(false)

        val xAxis: XAxis =    chart!!.xAxis
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawGridLines(false)
        xAxis.setDrawAxisLine(true)

        val leftAxis: YAxis =    chart!!.axisLeft
        leftAxis.setLabelCount(5, false)
        leftAxis.axisMinimum = 0f


        val rightAxis: YAxis =    chart!!.axisRight
        rightAxis.setLabelCount(5, false)
        rightAxis.setDrawGridLines(false)
        rightAxis.axisMinimum = 0f

        for (i in 0..29) {
            chart!!.data = generateDataLine(i + 1)
        }

        chart!!.animateX(750)

        return view
    }


    private fun generateDataLine(cnt: Int): LineData? {
        val values1 = ArrayList<Entry>()
        for (i in 0..11) {
            values1.add(
                Entry(
                    i.toFloat(),
                    ((Math.random() * 65).toInt() + 40).toFloat()
                )
            )
        }
        val d1 = LineDataSet(values1, "New DataSet $cnt, (1)")
        d1.lineWidth = 2.5f
        d1.circleRadius = 4.5f
        d1.highLightColor = Color.rgb(244, 117, 117)
        d1.setDrawValues(false)
        val values2 = ArrayList<Entry>()
        for (i in 0..11) {
            values2.add(Entry(i.toFloat(), values1[i].y - 30))
        }
        val d2 = LineDataSet(values2, "New DataSet $cnt, (2)")
        d2.lineWidth = 2.5f
        d2.circleRadius = 4.5f
        d2.highLightColor = Color.rgb(244, 117, 117)
        d2.color = ColorTemplate.VORDIPLOM_COLORS[0]
        d2.setCircleColor(ColorTemplate.VORDIPLOM_COLORS[0])
        d2.setDrawValues(false)
        val sets = ArrayList<ILineDataSet>()
        sets.add(d1)
        sets.add(d2)
        return LineData(sets)
    }

}