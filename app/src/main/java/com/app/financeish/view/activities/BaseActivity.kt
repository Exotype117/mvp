package com.app.financeish.view.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.app.financeish.R
import com.app.financeish.util.Constants

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)

        hideKeypad()

    }

    fun hideKeypad() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun gotoMainScreen() {
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }


    fun goToLoginScreen() {
        val intent =
            Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun goToForgotPasswordScreen() {
        val intent =
            Intent(this, ForgotPasswordActivity::class.java)
        startActivity(intent)
    }

    fun goToSignUpScreen() {
        val intent =
            Intent(this, SignUpActivity::class.java)
        startActivity(intent)
    }



    fun replaceFragment(fragment: Fragment) {
        var fragment = fragment
        val tag = fragment.javaClass.simpleName
        val tr: FragmentTransaction = supportFragmentManager.beginTransaction()
        val curFrag: Fragment? = supportFragmentManager.primaryNavigationFragment
        val cacheFrag: Fragment? = supportFragmentManager.findFragmentByTag(tag)
        if (curFrag != null) tr.hide(curFrag)
        if (cacheFrag == null) {
            tr.add(R.id.frame_layout, fragment, tag)
        } else {
            tr.show(cacheFrag)
            fragment = cacheFrag
        }
        tr.setPrimaryNavigationFragment(fragment)
        tr.commit()
    }
}