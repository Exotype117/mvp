package com.app.financeish.view.activities

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.app.financeish.R
import com.app.financeish.databinding.ActivitySignUpBinding


class SignUpActivity : BaseActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivitySignUpBinding>(
            this,
            R.layout.activity_sign_up
        )

        /**click listeners */
        binding.buttonSignIn.setOnClickListener(this)
        binding.buttonSignUp.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when(p0?.id){

            R.id.buttonSignIn ->{
                onBackPressed()
            }

            R.id.buttonSignUp ->{
                onBackPressed()
            }
        }
    }
}