import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.app.financeish.R
import com.app.financeish.databinding.LayoutAddDataBinding
import com.app.financeish.util.Constants
import com.app.financeish.view.fragments.BaseFragment


class AddDataFragment : BaseFragment(), View.OnClickListener {

    private lateinit var binding: LayoutAddDataBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(inflater, R.layout.layout_add_data, container, false)

        /**click listeners */
        binding.buttonIncome.setOnClickListener(this)
        binding.buttonDebts.setOnClickListener(this)
        binding.buttonExpenses.setOnClickListener(this)

        return binding.root


    }//onCreateView

    override fun onClick(p0: View?) {

        when (p0?.id) {
            R.id.buttonIncome -> {
                goToAddDataScreen(Constants.INCOME)
            }

            R.id.buttonDebts -> {
                goToAddDataScreen(Constants.DEBIT)
            }

            R.id.buttonExpenses -> {
                goToAddDataScreen(Constants.EXPENSES)
            }

        }
    }
}//AddDataFragment