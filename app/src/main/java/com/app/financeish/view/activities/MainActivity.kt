package com.app.financeish.view.activities

import AddDataFragment
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.app.financeish.R
import com.app.financeish.databinding.ActivityMainBinding
import com.app.financeish.view.fragments.HomeFragment
import com.app.financeish.view.fragments.ShowDataFragment
import com.etebarian.meowbottomnavigation.MeowBottomNavigation


class MainActivity : BaseActivity() {


    private val ID_HOME = 1
    private val ID_ADD = 2
    private val ID_SHOW = 3


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(
            this,
            R.layout.activity_main
        )

        binding.bottomNavigation.apply {
            add(
                MeowBottomNavigation.Model(
                    ID_HOME,
                    R.drawable.ic_home
                )
            )

            add(
                MeowBottomNavigation.Model(
                    ID_ADD,
                    R.drawable.ic_add
                )
            )

            add(
                MeowBottomNavigation.Model(
                    ID_SHOW,
                    R.drawable.ic_account
                )
            )


//            setOnClickMenuListener {
//                when (it.id) {
//                    ID_HOME -> replaceFragment(HomeFragment())
//                    ID_ADD -> replaceFragment(AddDataFragment())
//                    ID_SHOW -> replaceFragment(ShowDataFragment())
//                    else -> null
//                }
//            }

            setOnShowListener {
                when (it.id) {
                    ID_HOME -> replaceFragment(HomeFragment())
                    ID_ADD -> replaceFragment(AddDataFragment())
                    ID_SHOW -> replaceFragment(ShowDataFragment())
                    else -> null
                }

            }

            show(ID_HOME)

        }



    }
}//MainActivity