package com.app.financeish.view.activities

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import com.app.financeish.R
import com.app.financeish.databinding.ActivityAddDataBinding
import com.app.financeish.util.Constants

class AddDataActivity : BaseActivity(), View.OnClickListener {


    private var binding : ActivityAddDataBinding? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_data)
        /**click listeners */
        binding?.backButton?.setOnClickListener(this)
        binding?.buttonSubmit?.setOnClickListener(this)

        val title = intent.getStringExtra(Constants.TYPE)
        binding?.tvTitle?.text = title
        categoryAdapter()


    }

    override fun onClick(p0: View?) {
        when (p0?.id) {

            R.id.backButton -> {
                onBackPressed()
            }

            R.id.buttonSubmit ->{
                onBackPressed()
            }
        }
    }

    fun categoryAdapter(){

        var categoryAdapter = object : ArrayAdapter<String>(
            this, R.layout.spinner_layout, resources.getStringArray(R.array.category)) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }

        }
        categoryAdapter.setDropDownViewResource(R.layout.spinner_layout)
        binding?.spinnerCategory?.adapter = categoryAdapter

        binding?.spinnerCategory?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, view: View, position: Int, id: Long) {

            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }//sp_main
    }
}//AddDataActivity