package com.app.financeish.view.activities

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.app.financeish.R
import com.app.financeish.databinding.ActivityLoginBinding


class LoginActivity : BaseActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityLoginBinding>(
            this,
            R.layout.activity_login
        )

        /**click listeners */
        binding.buttonCreateAccount.setOnClickListener(this)
        binding.buttonLogin.setOnClickListener(this)
        binding.buttonForgotPassword.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {

        when (p0?.id) {
            R.id.buttonCreateAccount -> {
                goToSignUpScreen()
            }
            R.id.buttonLogin -> {
                gotoMainScreen()
            }
            R.id.buttonForgotPassword -> {
                goToForgotPasswordScreen()
            }

        }
    }
}