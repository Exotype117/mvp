package com.app.financeish.view.activities

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.app.financeish.R
import com.app.financeish.databinding.ActivityForgotPasswordBinding


class ForgotPasswordActivity : BaseActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityForgotPasswordBinding>(
            this,
            R.layout.activity_forgot_password
        )

        /**click listeners */
        binding.buttonRecoverPassword.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {

            R.id.buttonRecoverPassword -> {
                onBackPressed()
            }
        }
    }
}