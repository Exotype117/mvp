package com.app.financeish.view.fragments

import android.content.Intent
import androidx.fragment.app.Fragment
import com.app.financeish.util.Constants
import com.app.financeish.view.activities.AddDataActivity

abstract class BaseFragment : Fragment() {



    fun goToAddDataScreen(type: String) {
        val intent =
            Intent(requireContext(), AddDataActivity::class.java).apply {
                putExtra(Constants.TYPE, type)
            }
        startActivity(intent)
    }
}